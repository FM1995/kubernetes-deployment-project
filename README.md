# Kubernetes Deployment Project


### Pre-Requisites
minikube/kubectl installed

#### Project Outline

The current deployment of the "bootcamp-java-mysql" application (https://gitlab.com/FM1995/containers-with-docker-project), managed by Docker Compose on a server, is experiencing stability issues. Database and application containers frequently fail, requiring manual intervention to restart them. This results in downtime for users, including internal staff and company clients. To address this, the decision is made to enhance reliability and availability by adopting Kubernetes as a container orchestration tool. The goal is to replicate both the database and application containers to ensure a backup in case of failures. Additionally, the plan is to deploy the application across multiple servers to prevent issues arising from a single server failure or reboot. Initially, the configuration and deployment will be done manually to evaluate Kubernetes before automating the process.

## Table of Contents
- [Create a Kubernetes cluster](#create-a-kubernetes-cluster)
- [Deploy Mysql with 3 replicas](#deploy-mysql-with-3-replicas)
- [Deploy your Java Application with 3 replicas](#deploy-your-java-application-with-3-replicas)
- [Deploy phpmyadmin](#deploy-phpmyadmin)
- [Deploy Ingress Controller](#deploy-ingress-controller)
- [Create Ingress rule](#create-ingress-rule)
- [Port-forward for phpmyadmin](#port-forward-for-phpmyadmin)
- [Port-forward for phpmyadmin](#port-forward-for-phpmyadmin)


## Create a Kubernetes cluster

From the previous project we already have minikube installed so all we have to do is start it with a docker driver

https://gitlab.com/FM1995/install-minikube-2024

```
minikube start –driver docker
```
![Image1](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image1.png)

## Deploy Mysql with 3 replicas

First of all, you want to deploy the mysql database.

Deploy Mysql database with 3 replicas and volumes for data persistence

To simplify the process you can use Helm for that.

The first part is to install the helm repo bitnami

helm repo add bitnami https://charts.bitnami.com/bitnami

The command helm repo add bitnami https://charts.bitnami.com/bitnami tells Helm to add the Bitnami Helm chart repository to your Helm installation. 

This repository contains pre-packaged Helm charts for popular applications, like MySQL, Redis, and WordPress.

![Image2](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image2.png)

Can then check the repo list

![Image3](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image3.png)

Now lets create mysql-chart-values.yaml

![Image4](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image4.png)

Can now run the below

```
helm install mysql bitnami/mysql --values mysql-chart-values.yaml
```

The command helm install mysql bitnami/mysql --values mysql-chart-values.yaml will install the MySQL database on your Kubernetes cluster using the Bitnami Helm chart. The --values flag specifies the path to a YAML file that contains the values for the Helm chart. In this case, the YAML file is mysql-chart-values.yaml.

![Image5](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image5.png)

Can see the three pods are now running

```
kubectl get pod
```

![Image6](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image6.png)


## Deploy your Java Application with 3 replicas

Now we want to

•Deploy your Java application with 3 replicas.

With docker-compose, you were setting env_vars on server. In K8s there are own components for that, so

•Create ConfigMap and Secret with the values and reference them in the application deployment config file.
Solution

First lets create the registry key

Can set the registry credentials and create from there

```
kubectl create secret docker-registry my-registry-key \
--docker-server=$DOCKER_REGISTRY_SERVER \
--docker-username=$DOCKER_USER \
--docker-password=$DOCKER_PASSWORD \
--docker-email=$DOCKER_EMAIL
secret/my-registry-key created
and can see ‘my-registry-key’
```

![Image7](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image7.png)

Can also configure the db-secret.yaml

![Image8](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image8.png)

And should cross-check with my-sql-charts

Can the configure the db-config.yaml

![Image9](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image9.png)

Cross checked with the service

![Image10](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image10.png)

Can then create the java-deployment file using the docker image

![Image11](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image11.png)

Now composing all three in to it

my-registry-key, db-secret, and db-config

Deployment

![Image12](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image12.png)

Service

![Image13](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image13.png)

Now ready to apply the below

```
kubectl apply -f db-secret.yaml
```

![Image14](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image14.png)

```
kubectl apply -f db-config.yaml
```

![Image15](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image15.png)

And now ready to apply the java-app

```
kubectl apply -f java-app.yaml
```

![Image16](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image16.png)

## Deploy phpmyadmin

As a next step we want to deploy phpmyadmin to access Mysql UI.

For this deployment you just need 1 replica, since this is only for your own use, so it doesn't have to be High Availability. A simple deployment.yaml file and 
internal service will be enough.

Solution

Deployment

![Image17](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image17.png)

Service

![Image18](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image18.png)



```
kubectl apply -f phpmyadmin.yaml
```

![Image19](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image19.png)



Can check for the sql database

```
kubectl get svc
kubectl patch svc phpmyadmin-service -p '{"spec": {"type": "NodePort"}}'
kubectl patch svc phpmyadmin-service -p '{"spec": {"type": "ClusterIP"}}'
minikube service phpmyadmin-service --url
```

![Image20](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image20.png)

![Image21](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image21.png)

And able to access using the credentials in db-config

![Image22](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image22.png)


## Deploy Ingress Controller

Deploy Ingress Controller in the cluster - using Helm


minikube comes with ingress addon, so we just need to activate it


Can run the below command

```
minikube addons enable ingress
```

![Image23](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image23.png)

## Create Ingress rule

Create Ingress rule for your application access

Solution

We can generate the yaml file for java-app-ingress functionality

First we need to add the minikube to host IP


![Image24](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image24.png)

![Image25](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image25.png)

![Image26](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image26.png)

Now we are ready to configure the java-app-ingress functionality

Also as an alternative, can run the following and change the node port

```
kubectl patch svc java-app-service -p '{"spec": {"type": "NodePort"}}'
kubectl patch svc java-app-service -p '{"spec": {"type": "ClusterIP"}}'
```

and then run

```
minikube service java-app-service –url
```

And can now access the application on the below

```
http://127.0.0.1:62111
```

![Image27](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image27.png)

![Image28](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image28.png)

Getting the below

```
kubectl apply -f java-app-ingress.yaml
```

![Image29](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image29.png)

Let’s edit the ingress file

![Image30](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image30.png)

![Image31](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image31.png)

```
kubectl get ingress java-app-ingress
```

![Image32](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image32.png)


## Port-forward for phpmyadmin

However, you don't want to expose the phpmyadmin for security reasons. So you configure port-forwarding for the service to access on localhost, whenever you need it.

Configure port-forwarding for phpmyadmin

Can run the below

kubectl port-forward svc/phpmyadmin-service 8081:8081

![Image33](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image33.png)

Can access the database on http://localhost:8081/

![Image34](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image34.png)
![Image35](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image35.png)


## Create Helm Chart for Java App

As the final step, we decided to create a helm chart for your Java application where all the configuration files are configurable. We can then tell developers how they can use it by setting all the chart values. This chart will be hosted in its own git repository.

Steps
•	All config files: service, deployment, ingress, configMap, secret, will be part of the chart
•	Create custom values file as an example for developers to use when deploying the application
•	Deploy the java application using the chart with helmfile
•	Host the chart in its own git repository

Solution:

First we will start with the below command

```
helm create java-app
```

![Image36](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image36.png)

Going in further in to the file directory can see the new file generation

![Image37](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image37.png)

This is how the current directory is looking

![Image38](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image38.png)

Now time to clean up

![Image39](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image39.png)

![Image40](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image40.png)

Now adding the relevant files for deployment, ingress, service, de-secret, db-config

Finalised file setup

![Image41](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image41.png)

Can fill in the values data with default values

![Image42](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image42.png)

Setting the correct values for the values

![Image43](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image43.png)

Now ready to apply the helm chart

![Image44](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image44.png)

Now ready to test the installation using the dry run, if dry-run shows the k8s manifest files with correct values, everything is working, so you can create the chart release

Using the cmd

```
helm install --dry-run --values values-override.yaml my-awesome-java-app $(pwd)
```

![Image45](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image45.png)

![Image46](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image46.png)

Can now finally install it

```
helm install my-cool-java-app -f values-override.yaml .
```

![Image47](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image47.png)

Can see the java-app deployed

![Image48](https://gitlab.com/FM1995/kubernetes-deployment-project/-/raw/main/Images/Image48.png)




